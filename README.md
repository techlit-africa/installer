# TechLit Installer

WARNING: this repo is changing rapidly, so by default, you should ask Tyler

## Getting an TLI liveboot ISO

1. install `VirtualBox` and `vagrant`
1. from inside `./archiso`, run `vagrant up --provision` (_NOTE_: sometimes the ssh connection fails on the first try)
1. if the previous command succeeds, you'll find a new `techlit-installer-*.iso` file in the
   `./archiso` directory
1. _BONUS_: you can use `dd` to copy the `iso` file to a usb

## TODO

- the `/tli` repo on the liveboot image should update itself if online
- the liveboot should connect to known networks automatically
- the liveboot needs an intro message

- the cli needs to use the new `cli_parser.rb` to parse args
- the cli needs to use args rather than a config file
- the stages / tasks need to be composed more declaratively (not procedurally), so that the user can have more control over what runs
- the cli needs `start`, `stop`, `only` and `except` flags to control which stages / tasks run

- mint installations should have the liveboot embedded after the boot partitions

- figure out image storage
  - we could have a way to install liveboot next to storage (like a usb sidecar)
  - we could have images be copyable from some network storage (like sftp)
