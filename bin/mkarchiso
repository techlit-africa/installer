#!/usr/bin/env bash

set -e

copy_configs() {
  cp -r /usr/share/archiso/configs/releng/* setup/

  echo jq >> setup/packages.x86_64
  echo ruby >> setup/packages.x86_64
  echo dialog >> setup/packages.x86_64
  sed -i '/^reflector$/d' setup/packages.x86_64
  cat setup/packages.x86_64

  sed -i 's/iso_name=".*"/iso_name="techlit-installer"/' setup/profiledef.sh
  sed -i 's/iso_label=".*"/iso_label="TLI_'$1'"/' setup/profiledef.sh
  sed -i 's/iso_publisher=".*"/iso_publisher="TechLit"/' setup/profiledef.sh
  sed -i 's/iso_application=".*"/iso_application="TechLit Installer"/' setup/profiledef.sh

  echo 'LANG=en_US.UTF-8' > setup/airootfs/etc/locale.conf
  echo 'en_US.UTF-8 UTF-8' > setup/airootfs/etc/locale.gen

  rm setup/airootfs/etc/systemd/system/multi-user.target.wants/pacman-init.service
  rm setup/airootfs/etc/systemd/system/multi-user.target.wants/choose-mirror.service
  rm setup/airootfs/etc/systemd/system/multi-user.target.wants/ModemManager.service
  rm setup/airootfs/etc/systemd/system/multi-user.target.wants/sshd.service
  rm setup/airootfs/etc/systemd/system/multi-user.target.wants/systemd-networkd.service
  rm setup/airootfs/etc/systemd/system/multi-user.target.wants/systemd-resolved.service
  rm setup/airootfs/etc/systemd/system/multi-user.target.wants/iwd.service
  rm setup/airootfs/etc/systemd/system/multi-user.target.wants/reflector.service
}


install_scripts() {
  mkdir -p setup/airootfs/data
  mkdir -p setup/airootfs/root/bin
  cp -R ../bin/* setup/airootfs/root/bin

  echo :> setup/airootfs/etc/motd

  cat <<-ZSH > setup/airootfs/root/.zshrc
export PATH="\$PATH:\$HOME/bin"

_say=\$(tput setaf 3)
_run=\$(tput setaf 2)
_shout=\$(tput setaf 6)
_std=\$(tput sgr0)
say() { echo -e "\$_say\n# \$*\$_std"; }
run() { sleep 0.1; echo -e "\$_say\\\$ \$_run\$*\$_std"; sleep 0.1; \$*; sleep 0.1; }

say Generating Locales
run locale-gen

if ! mount | grep /data > /dev/null; then
  say Mounting /dev/sdx4 on /data

  iso=\$(mount | grep /run/archiso/bootmnt | cut -d' ' -f1)
  run mount \${iso::-1}4 /data
fi

say Attached block devices
run lsblk

say Contents of /data
run ls -alh /data

say Instructions
echo "\$_say#
\$_say#   Welcome to the TechLit recovery system -- Version $1
\$_say#
\$_say#   Example commands:
\$_say#
\$_say#     Destroy hard drive (one-time per machine)
\$_say#       \${_run}tl-hw-destroy  \${_shout}/dev/sdx
\$_say#
\$_say#     Partition hard drive (one-time per machine)
\$_say#       \${_run}tl-hw-prepare  \${_shout}/dev/sdx  /mnt
\$_say#
\$_say#     Update & migrate TechLit OS (every machine for every update)
\$_say#       \${_run}tl-hw-update  \${_shout}/dev/sdx  /mnt  /data/0.0.0.tar.gz
\$_say#
\$_say#     Save a snapshot of TechLit OS (to create a new update)
\$_say#       \${_run}tl-img-save  \${_shout}/dev/sdxy  /mnt  0.0.0  /data
\$_say#"
ZSH

  for file in ../bin/*; do
    sed -i "/^)$/i \ \ [\"/root/bin/$(basename $file)\"]=\"0:0:755\"" setup/profiledef.sh
  done

  chown -R 0:0 build
}

main() {
  [[ -d archiso ]] || mkdir -p archiso/{setup,build,out}

  pushd archiso
  trap popd 0

  copy_configs "${1:?}"
  install_scripts "${1:?}"

  mkarchiso -v -w build -o out setup
  mv out/*.iso ../$1.iso
}

[[ $0 == $BASH_SOURCE ]] && main "${1:?}"
